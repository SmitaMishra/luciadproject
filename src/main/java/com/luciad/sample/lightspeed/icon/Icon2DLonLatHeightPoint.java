package com.luciad.sample.lightspeed.icon;

import com.luciad.shape.shape3D.TLcdLonLatHeightPoint;
import com.luciad.util.ILcdOriented;

public class Icon2DLonLatHeightPoint extends TLcdLonLatHeightPoint implements ILcdOriented {

	private double fOrientation;

	public Icon2DLonLatHeightPoint() {
		super();
	}

	public Icon2DLonLatHeightPoint(double aLon, double aLat, double aHeight, double aOrientation) {
		super(aLon, aLat, aHeight);
		fOrientation = aOrientation;
	}

	public Icon2DLonLatHeightPoint(double aLon, double aLat, double aHeight) {
		this(aLon, aLat, aHeight, 0.0);
	}

	@Override
	public double getOrientation() {
		return fOrientation;
	}

	public void setOrientation(double aOrientation) {
		fOrientation = aOrientation;
	}

}
