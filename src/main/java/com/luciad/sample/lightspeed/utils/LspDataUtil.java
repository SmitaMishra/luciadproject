/*
 *
 * Copyright (c) 1999-2018 Luciad All Rights Reserved.
 *
 * Luciad grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Luciad.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. LUCIAD AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL LUCIAD OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF LUCIAD HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */
package com.luciad.sample.lightspeed.utils;

import java.awt.Component;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.luciad.gui.TLcdAWTUtil;
import com.luciad.model.ILcdModel;
import com.luciad.model.ILcdModelDecoder;
import com.luciad.model.TLcdCompositeModelDecoder;
import com.luciad.util.TLcdNoBoundsException;
import com.luciad.util.TLcdOutOfBoundsException;
import com.luciad.view.lightspeed.ALspViewAdapter;
import com.luciad.view.lightspeed.ILspAWTView;
import com.luciad.view.lightspeed.ILspView;
import com.luciad.view.lightspeed.layer.ILspLayer;
import com.luciad.view.lightspeed.layer.ILspLayerFactory;
import com.luciad.view.lightspeed.layer.TLspCompositeLayerFactory;
import com.luciad.view.lightspeed.layer.TLspPaintRepresentation;
import com.luciad.view.lightspeed.painter.grid.TLspLonLatGridLayerBuilder;
import com.luciad.view.lightspeed.util.TLspViewNavigationUtil;
import com.luciad.view.opengl.binding.ILcdGLDrawable;

public class LspDataUtil {

	private String fSource;
	private ILcdModel fModel;
	private ILspLayer fLayer;
	private ILspView fView;

	public static LspDataUtil instance() {
		return new LspDataUtil();
	}

	/**
	 * Takes the given model as input.
	 */
	public LspDataUtil model(ILcdModel aSource) {
		fModel = aSource;
		return this;
	}

	/**
	 * Creates a Lon Lat grid layer.
	 */
	public LspDataUtil grid() {
		fLayer = TLspLonLatGridLayerBuilder.newBuilder().build();
		return this;
	}

	/**
	 * Decodes the given source, optionally using the given model decoders.
	 */
	public LspDataUtil model(String aSource, ILcdModelDecoder... aDecoders) {
		fSource = aSource;
		TLcdCompositeModelDecoder decoder = new TLcdCompositeModelDecoder(aDecoders);
		try {
			fModel = decoder.decode(aSource);
		} catch (IOException e) {
			throw new RuntimeException("Could not decode " + aSource, e);
		}
		return this;
	}

	/**
	 * Creates a layer for the set model, optionally using the given layer
	 * factories.
	 */
	public LspDataUtil layer(ILspLayerFactory... aLayerFactories) {
		checkNotNull(fModel, "Specify a model before calling the layer method");
		TLspCompositeLayerFactory factory = new TLspCompositeLayerFactory(aLayerFactories);
		Collection<ILspLayer> layers = factory.createLayers(fModel);
		checkNotNull(layers,
				"Could not create a layer for " + fModel.getModelDescriptor().getSourceName() + ". Make sure "
						+ "that the given layer factory can create a layer for the model, or (when not specifying a "
						+ "layer factory) that annotation processing is enabled.");
		fLayer = layers.iterator().next();
		return this;
	}

	/**
	 * Changes the label of the created layer.
	 */
	public LspDataUtil label(String aLabel) {
		checkNotNull(fLayer, "Create a layer before calling the label method");
		fLayer.setLabel(aLabel);
		return this;
	}

	/**
	 * Determines whether the created layer is selectable or not.
	 */
	public LspDataUtil selectable(boolean aSelectable) {
		checkNotNull(fLayer, "Create a layer before calling the selectable method");
		fLayer.setSelectable(aSelectable);
		return this;
	}

	public LspDataUtil editable(boolean aEditable) {
		checkNotNull(fLayer, "Create a layer before calling the editable method");
		fLayer.setEditable(aEditable);
		return this;
	}

	public LspDataUtil labeled(boolean aLabeled) {
		checkNotNull(fLayer, "Create a layer before calling the labeled method");
		fLayer.setVisible(TLspPaintRepresentation.LABEL, aLabeled);
		return this;
	}

	public LspDataUtil addToView(final ILspView aView) {
		if (fLayer == null) {
			layer();
		}
		fView = aView;
		TLcdAWTUtil.invokeAndWait(new Runnable() {
			@Override
			public void run() {
				aView.addLayer(fLayer);
			}
		});
		return this;
	}

	public LspDataUtil fit() {
		checkNotNull(fLayer, "Create a layer before calling the fit method");
		fitOnLayers(null, fView, false, fLayer);
		return this;
	}

	public String getSource() {
		return fSource;
	}

	public ILcdModel getModel() {
		return fModel;
	}

	public ILspLayer getLayer() {
		return fLayer;
	}

	private void checkNotNull(Object aValue, String aReason) {
		if (aValue == null) {
			throw new IllegalArgumentException(aReason);
		}
	}

	public static void fitOnLayers(final Component aComponentForFailMessage, final ILspView aView,
			final boolean aAnimatedFit, final ILspLayer... aLayers) {
		if (aLayers != null && aLayers.length > 0) {
			boolean fit = false;
			for (ILspLayer layer : aLayers) {
				if (layer.isVisible()) {
					fit = true;
				}
			}
			if (fit) {
				TLcdAWTUtil.invokeNowOrLater(new Runnable() {
					public void run() {
						// At startup the view's width and height might not
						// have been initialized. To make sure that these
						// have been set properly, fitting is performed
						// at the end of the view's repaint by using an
						// ILspViewListener.
						aView.addViewListener(new ALspViewAdapter() {
							@Override
							public void preRender(ILspView aView, ILcdGLDrawable aGLDrawable) {
								aView.removeViewListener(this);
								try {
									if (aAnimatedFit) {
										new TLspViewNavigationUtil(aView).animatedFit(Arrays.asList(aLayers));
									} else {
										new TLspViewNavigationUtil(aView).fit(aLayers);
									}
								} catch (TLcdOutOfBoundsException e) {
									showMessageDialog(aView, aComponentForFailMessage,
											"Layer not visible in current projection.");
								} catch (final TLcdNoBoundsException e) {
									showMessageDialog(aView, aComponentForFailMessage,
											"Could not fit on the layer.\n" + e.getMessage());
								}
							}
						});
					}
				});
			}
		}
	}

	private static void showMessageDialog(ILspView aView, final Component aComponent, final String aMessage) {
		if (aView instanceof ILspAWTView) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					JOptionPane.showMessageDialog(aComponent, aMessage);
				}
			});
		}
	}
}