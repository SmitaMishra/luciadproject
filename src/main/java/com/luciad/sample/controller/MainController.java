package com.luciad.sample.controller;

import java.awt.Component;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import javax.swing.JComponent;
import javax.swing.JLabel;

import com.luciad.earth.model.TLcdEarthRepositoryModelDecoder;
import com.luciad.geodesy.TLcdGeodeticDatum;
import com.luciad.gui.swing.TLcdOverlayLayout;
import com.luciad.model.ILcdModel;
import com.luciad.model.TLcdModelDescriptor;
import com.luciad.model.TLcdVectorModel;
import com.luciad.projection.TLcdEquidistantCylindrical;
import com.luciad.reference.ILcdGridReference;
import com.luciad.reference.TLcdGeodeticReference;
import com.luciad.reference.TLcdGridReference;
import com.luciad.sample.lightspeed.action.SwingControllerActions;
import com.luciad.sample.lightspeed.balloon.BalloonContentProvider;
import com.luciad.sample.lightspeed.balloon.BalloonViewSelectionListener;
import com.luciad.sample.lightspeed.factory.BasicLayerFactory;
import com.luciad.sample.lightspeed.factory.EditableLayerFactory;
import com.luciad.sample.lightspeed.icon.IconSelectionEventListener;
import com.luciad.sample.lightspeed.mouse.MouseLocationComponent;
import com.luciad.sample.lightspeed.utils.FXLayerControl;
import com.luciad.sample.lightspeed.utils.InitialLayerIndexProvider;
import com.luciad.sample.lightspeed.utils.LayerPaintExceptionHandler;
import com.luciad.sample.lightspeed.utils.LspDataUtil;
import com.luciad.sample.lightspeed.utils.UIColors;
import com.luciad.sample.media.MediaControl;
import com.luciad.util.TLcdNoBoundsException;
import com.luciad.util.TLcdOutOfBoundsException;
import com.luciad.util.measure.ILcdLayerMeasureProviderFactory;
import com.luciad.util.measure.ILcdModelMeasureProviderFactory;
import com.luciad.view.lightspeed.ILspView;
import com.luciad.view.lightspeed.TLspSwingView;
import com.luciad.view.lightspeed.TLspViewBuilder;
import com.luciad.view.lightspeed.layer.ILspInteractivePaintableLayer;
import com.luciad.view.lightspeed.layer.ILspLayer;
import com.luciad.view.lightspeed.layer.ILspLayerFactory;
import com.luciad.view.lightspeed.layer.TLspCompositeLayerFactory;
import com.luciad.view.lightspeed.swing.TLspBalloonManager;
import com.luciad.view.lightspeed.swing.TLspScaleIndicator;
import com.luciad.view.lightspeed.swing.navigationcontrols.TLspNavigationControlsBuilder;
import com.luciad.view.lightspeed.util.TLspViewNavigationUtil;
import com.luciad.view.lightspeed.util.TLspViewTransformationUtil;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class MainController {

	@FXML
	private BorderPane mapViewPane;
	@FXML
	private SwingNode treeViewSwingNode;
	@FXML
	private Button recenterButton, rulerButton, redoButton, undoButton, defaultButton;
	@FXML
	private Button pinButton;
	@FXML
	private ToggleButton dimensionButton;

	public boolean isPinToggleSelected;

	private TLspSwingView fView;
	TLspBalloonManager fBalloonManager;
	ILspInteractivePaintableLayer fInteractiveLayer;
	ILcdModel fInteractiveModel;

	SwingControllerActions controllers;

	public MainController() {
		super();
	}

	@FXML
	private void initialize() throws IOException {
		//
		initializeMapView();

		initializeViews();
	}

	@FXML
	protected void closeWindow(ActionEvent event) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				getView().destroy();
				System.exit(0);
			}
		});
	}

	@FXML
	protected void openVideoOptions(ActionEvent event) {

		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/main_page.fxml"));
			Parent root = loader.load();

			Stage stage = new Stage();
			stage.setTitle("Cyclop Video Player");

			Media m = new Media("file:///" + System.getProperty("user.dir").replace('\\', '/') + "/" + "res/UGV.mp4");
			MediaPlayer player = new MediaPlayer(m);

			Scene scene = new Scene(root, 1280, 800);
			MediaControl mediaControl = new MediaControl(player);
			scene.setRoot(mediaControl);

			stage.setScene(scene);
			stage.show();

			stage.setOnCloseRequest(ev -> player.stop());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initializeViews() {
		// Recenter Button
		recenterButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				controllers.getRecenterButton().doClick();
			}
		});
		recenterButton.setTooltip(new Tooltip("Recenter View"));

		// Enable the button for grid references only.
		recenterButton.setDisable(!(fView.getXYZWorldReference() instanceof ILcdGridReference));

		fView.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getSource() instanceof ILspView
						&& "xyzworldreference".equalsIgnoreCase(evt.getPropertyName())) {
					// check this?
					// Platform.runLater(() -> {
					recenterButton.setDisable(
							!(((ILspView) evt.getSource()).getXYZWorldReference() instanceof ILcdGridReference));
					// });
				}
			}
		});

		pinButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				controllers.getPinButton().doClick();
			}
		});
		pinButton.setTooltip(new Tooltip("Insert Point"));

		rulerButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				controllers.getRulerButton().doClick();
			}
		});
		rulerButton.setTooltip(new Tooltip("Measure Distance"));

		undoButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				controllers.getUndoButton().doClick();
			}
		});
		undoButton.setTooltip(new Tooltip("Undo Action"));

		redoButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				controllers.getRedoButton().doClick();
			}
		});
		redoButton.setTooltip(new Tooltip("Redo Action"));

		defaultButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				controllers.getDefaultCursor().doClick();
			}
		});
		defaultButton.setTooltip(new Tooltip("Cursor"));
	}

	public TLspSwingView getView() {
		return fView;
	}

	protected void initializeMapView() throws IOException {
		fView = createView();
		fView.setLayerFactory(createLayerFactory());

		// Add Map Controllers
		initControllers(fView);

		// Add Data
		initLayers(fView);

		// ToolBar including 2D/3D
		ToolBar toolbar = createToolBar(fView);
		toolbar.setPadding(new Insets(10, 10, 10, 10));

		SwingNode viewNode = new SwingNode();
		viewNode.setContent(fView.getHostComponent());

		// Do the lay-out using FX
		VBox vbox = new VBox(toolbar, viewNode);
		VBox.setVgrow(viewNode, Priority.ALWAYS);
		VBox.setVgrow(toolbar, Priority.NEVER);

		mapViewPane.setCenter(vbox);

		// Add the overlay components.
		if (fView.getOverlayComponent() != null) {
			addOverlayComponents(getOverlayPanel());
		}

		// Add Layer Controls - swing component
		final JFXPanel eastPanel = new JFXPanel();
		eastPanel.setPreferredSize(new Dimension(400, 300));
		FXLayerControl layerControl = new FXLayerControl(getView());
		eastPanel.setScene(new Scene(layerControl));
		treeViewSwingNode.setContent(eastPanel);

	}

	protected void initLayers(TLspSwingView aView) throws IOException {

		LspDataUtil.instance()
				.model("Data/Earth/SanFrancisco/tilerepository.cfg", new TLcdEarthRepositoryModelDecoder())
				.layer(new BasicLayerFactory()).label("World").addToView(getView()).fit();

		// Create and add the grid layer
		LspDataUtil.instance().grid().addToView(getView());


		// focus the view on Qatar
		// final double LON = 48;
		// final double LAT = 24; 48,24,10,13

		/*
		 * aView.addViewListener(new ALspViewAdapter() {
		 * 
		 * @Override public void preRender(ILspView aView, ILcdGLDrawable aGLDrawable) {
		 * aView.removeViewListener(this); try { new
		 * TLspViewNavigationUtil(aView).animatedFitOnModelBounds(new
		 * TLcdLonLatBounds(-8, -8, 16, 16), new TLcdGeodeticReference()); } catch
		 * (TLcdOutOfBoundsException e) { e.printStackTrace(); } } });
		 */
	}

	protected void fitViewExtents(TLspSwingView aView, Collection<ILspLayer> aLayers) {
		try {
			// Fit the view to the relevant layers.
			new TLspViewNavigationUtil(aView).fit(aLayers);
		} catch (TLcdOutOfBoundsException e) {
			e.printStackTrace();
		} catch (TLcdNoBoundsException e) {
			e.printStackTrace();
		}
	}

	protected ILspInteractivePaintableLayer createEmptyModel(TLspSwingView aView) {
		TLcdGeodeticDatum geodeticDatum = new TLcdGeodeticDatum();
		TLcdGeodeticReference modelReference = new TLcdGeodeticReference(geodeticDatum);
		fInteractiveModel = new TLcdVectorModel(modelReference,
				new TLcdModelDescriptor("Layer containing the newly created shapes.", "Shapes", "Geodetic Shapes"));
		return (ILspInteractivePaintableLayer) LspDataUtil.instance().model(fInteractiveModel)
				.layer(new EditableLayerFactory()).editable(true).label("Custom Objects View").addToView(aView)
				.getLayer();
	}

	protected ToolBar createToolBar(final ILspView aView) {

		// Create and add toolbar to frame
		ToolBar toolBar = new ToolBar();

		// Create a button group for the radio buttons
		RadioButton b2d = new RadioButton("2D");
		b2d.setSelected(true);
		b2d.setTooltip(new Tooltip("Switch the view to 2D"));
		b2d.setUserData("2D");

		RadioButton b3d = new RadioButton("3D");
		b3d.setTooltip(new Tooltip("Switch the view to 3D"));
		b3d.setUserData("3D");

		final ToggleGroup toggleGroup = new ToggleGroup();
		b2d.setToggleGroup(toggleGroup);
		b3d.setToggleGroup(toggleGroup);

		toggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
				if (newValue.getUserData().equals("2D")) {
					TLspViewTransformationUtil.setup2DView(aView,
							new TLcdGridReference(new TLcdGeodeticDatum(), new TLcdEquidistantCylindrical()), true);
				} else if (newValue.getUserData().equals("3D")) {
					TLspViewTransformationUtil.setup3DView(aView, true);
				}
			}
		});

		// Add the two buttons to the toolbar
		toolBar.getItems().add(b2d);
		toolBar.getItems().add(b3d);

		// Add a button for drawing lines
		Button lineButton = new Button();
		File file = new File("res/images/line.png");
		lineButton.setGraphic(new ImageView(new Image(file.toURI().toString())));
		lineButton.setTooltip(new Tooltip("Draw Polyline"));
		lineButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent a) {
				controllers.getPolylineButton().doClick();
			}
		});

		Button squareButton = new Button();
		squareButton.setGraphic(new ImageView(new Image((new File("res/images/square.png")).toURI().toString())));
		squareButton.setTooltip(new Tooltip("Draw Square"));
		squareButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent a) {
				controllers.getBoundsButton().doClick();
			}
		});

		Button circleButton = new Button();
		circleButton.setGraphic(new ImageView(new Image((new File("res/images/circle.png")).toURI().toString())));
		circleButton.setTooltip(new Tooltip("Draw Circle"));
		circleButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent a) {
				controllers.getCircleButton().doClick();
			}
		});

		ToggleButton extrusionButton = new ToggleButton();
		extrusionButton.setGraphic(new ImageView(new Image((new File("res/images/3d-view.png")).toURI().toString())));
		extrusionButton.setTooltip(new Tooltip("Extrude Object"));
		extrusionButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent a) {
				controllers.getExtrusionButton().doClick();
			}
		});

		Button animationButton = new Button();
		animationButton.setGraphic(new ImageView(new Image((new File("res/images/road.png")).toURI().toString())));
		animationButton.setTooltip(new Tooltip("Animate Object"));
		animationButton.setOnAction(new IconSelectionEventListener(fInteractiveModel, fInteractiveLayer, 100000));

		toolBar.getItems().add(new Separator());
		toolBar.getItems().add(lineButton);
		toolBar.getItems().add(squareButton);
		toolBar.getItems().add(circleButton);
		toolBar.getItems().add(extrusionButton);
		toolBar.getItems().add(new Separator());
		toolBar.getItems().add(animationButton);
		toolBar.getItems().add(new Separator());

		return toolBar;
	}

	public JComponent getOverlayPanel() {
		if ((fView.getOverlayComponent() != null) && (fView.getOverlayComponent() instanceof JComponent)) {
			return (JComponent) fView.getOverlayComponent();
		} else {
			return null;
		}
	}

	protected TLspSwingView createView() {
		return createView(ILspView.ViewType.VIEW_2D);
	}

	protected TLspSwingView createView(ILspView.ViewType aViewType) {
		TLspSwingView view = TLspViewBuilder.newBuilder().viewType(aViewType).background(UIColors.bgMap())
				.addAtmosphere(true).paintExceptionHandler(new LayerPaintExceptionHandler()).buildSwingView();
		view.getRootNode().setInitialLayerIndexProvider(new InitialLayerIndexProvider());
		return view;
	}

	protected ILspLayerFactory createLayerFactory() {
		// Pick up all other layer factories
		return new TLspCompositeLayerFactory(new BasicLayerFactory(), new EditableLayerFactory());
	}

	protected void addOverlayComponents(JComponent aOverlayPanel) {
		addNavigationControls(aOverlayPanel);
		addAltitudeExaggerationControl(aOverlayPanel);

		TLcdOverlayLayout layout = (TLcdOverlayLayout) aOverlayPanel.getLayout();

		TLspScaleIndicator scaleIndicator = new TLspScaleIndicator(fView);
		scaleIndicator.setScaleAtCenterOfMap(true);
		JLabel scaleIndicatorLabel = scaleIndicator.getLabel();
		aOverlayPanel.add(scaleIndicatorLabel);
		layout.putConstraint(scaleIndicatorLabel, TLcdOverlayLayout.Location.SOUTH_EAST,
				TLcdOverlayLayout.ResolveClash.VERTICAL);

		if (getView() instanceof TLspSwingView) {

			Iterable<ILcdModelMeasureProviderFactory> measureProviderFactories = Collections
					.<ILcdModelMeasureProviderFactory>emptyList();
			Iterable<ILcdLayerMeasureProviderFactory> layerMeasureProviderFactories = Collections
					.<ILcdLayerMeasureProviderFactory>emptyList();
			aOverlayPanel.add(new MouseLocationComponent((TLspSwingView) getView(), measureProviderFactories,
					layerMeasureProviderFactories), TLcdOverlayLayout.Location.SOUTH);
		}
	}

	protected void addNavigationControls(JComponent aOverlayPanel) {
		TLcdOverlayLayout layout = (TLcdOverlayLayout) aOverlayPanel.getLayout();
		Component navigationControls = TLspNavigationControlsBuilder.newBuilder(getView()).build();
		aOverlayPanel.add(navigationControls);
		layout.putConstraint(navigationControls, TLcdOverlayLayout.Location.NORTH_EAST,
				TLcdOverlayLayout.ResolveClash.VERTICAL);
	}

	protected void addAltitudeExaggerationControl(JComponent aOverlayPanel) {
		JComponent altitudeExaggerationControl = (JComponent) TLspNavigationControlsBuilder.newBuilder(getView())
				.altitudeExaggerationControl().build();
		aOverlayPanel.add(altitudeExaggerationControl, TLcdOverlayLayout.Location.WEST);
	}

	protected void initControllers(TLspSwingView aView) {

		// Add Balloon Manager
		fBalloonManager = new TLspBalloonManager(getView(), getOverlayPanel(), TLcdOverlayLayout.Location.NO_LAYOUT,
				new BalloonContentProvider());

		BalloonViewSelectionListener listener = new BalloonViewSelectionListener(getView(), fBalloonManager);
		getView().addLayeredListener(listener);
		getView().addLayerSelectionListener(listener);
		getView().getRootNode().addHierarchyPropertyChangeListener(listener);

		// create ILspInteractivePaintableLayer
		fInteractiveLayer = createEmptyModel(aView);

		// SwingControllerActions
		controllers = new SwingControllerActions(aView, fInteractiveLayer);

	}

}
