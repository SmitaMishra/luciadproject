package com.luciad.sample.lightspeed.action;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Vector;

import com.luciad.datamodel.ILcdDataObject;
import com.luciad.model.ILcdModel;
import com.luciad.util.ILcdChangeListener;
import com.luciad.util.ILcdChangeSource;
import com.luciad.util.ILcdDynamicFilter;
import com.luciad.util.ILcdFilter;
import com.luciad.util.TLcdChangeEvent;
import com.luciad.util.TLcdChangeSupport;
import com.luciad.view.ALcdObjectSelectionAction;
import com.luciad.view.ILcdView;
import com.luciad.view.TLcdDomainObjectContext;
import com.luciad.view.lightspeed.TLspSwingView;

public class ShowPropertiesAction extends ALcdObjectSelectionAction {

	private Component fParentComponent;

	public ShowPropertiesAction(TLspSwingView aView, Component aParentComponent) {
		this(aView, aParentComponent, null);
	}

	/**
	 * Creates an edit selection action with a default tree cell renderer.
	 * 
	 * @param aView            a view
	 * @param aParentComponent the parent component containing the action
	 */
	public ShowPropertiesAction(ILcdView aView, Component aParentComponent,
			ILcdFilter<TLcdDomainObjectContext> aObjectFilter) {
		super(aView, getObjectFilter(aObjectFilter), 1, // minimum selection count
				1, // maximum selection count
				true); // strict
		setName("View properties");
		fParentComponent = aParentComponent;
	}

	public ShowPropertiesAction(ILcdView aView, ILcdFilter<TLcdDomainObjectContext> aObjectFilter) {
		super(aView, getObjectFilter(aObjectFilter), 1, // minimum selection count
				1, // maximum selection count
				true); // strict
		setName("View properties");
	}

	private static ILcdFilter<TLcdDomainObjectContext> getObjectFilter(
			ILcdFilter<TLcdDomainObjectContext> aObjectFilter) {
		ILcdFilter<TLcdDomainObjectContext> dataObjectFilter = new ILcdFilter<TLcdDomainObjectContext>() {
			@Override
			public boolean accept(TLcdDomainObjectContext aObject) {
				return aObject.getDomainObject() instanceof ILcdDataObject;
			}
		};
		if (aObjectFilter == null) {
			return dataObjectFilter;
		} else {
			CompositeAndFilter<TLcdDomainObjectContext> compositeAndFilter = new CompositeAndFilter<TLcdDomainObjectContext>();
			compositeAndFilter.addFilter(dataObjectFilter);
			compositeAndFilter.addFilter(aObjectFilter);
			return compositeAndFilter;
		}
	}

	@Override
	protected void actionPerformed(ActionEvent aActionEvent, List<TLcdDomainObjectContext> aSelection) {
		if (aSelection.size() > 0) {
			TLcdDomainObjectContext domainObjectContext = aSelection.get(0);
			ILcdDataObject dataObject = (ILcdDataObject) domainObjectContext.getDomainObject();
			ILcdModel model = domainObjectContext.getModel();

		}
	}

	private static class CompositeAndFilter<T> implements ILcdDynamicFilter<T> {

		private Vector<ILcdFilter<T>> fFilters = new Vector<ILcdFilter<T>>();
		private TLcdChangeSupport fChangeSupport = new TLcdChangeSupport();
		private MyChangeListener fMyChangeListener = new MyChangeListener();

		public CompositeAndFilter() {
		}

		@Override
		public boolean accept(T aObject) {
			for (int i = 0, c = getFilterCount(); i < c; i++) {
				if (!getFilter(i).accept(aObject)) {
					return false;
				}
			}
			return true;
		}

		public void addFilter(ILcdFilter<T> aFilter) {
			if (aFilter == null) {
				throw new NullPointerException("Cannot add null as a filter");
			}
			fFilters.add(aFilter);
			if (aFilter instanceof ILcdChangeSource) {
				((ILcdChangeSource) aFilter).removeChangeListener(fMyChangeListener);
				((ILcdChangeSource) aFilter).addChangeListener(fMyChangeListener);
			}
			fChangeSupport.fireChangeEvent(new TLcdChangeEvent(this));
		}

		public void removeFilter(ILcdFilter<T> aFilter) {
			if (aFilter != null) {
				if (fFilters.remove(aFilter)) {
					fChangeSupport.fireChangeEvent(new TLcdChangeEvent(this));
				}
				if (aFilter instanceof ILcdChangeSource) {
					((ILcdChangeSource) aFilter).removeChangeListener(fMyChangeListener);
				}
			}
		}

		public int getFilterCount() {
			return fFilters.size();
		}

		public ILcdFilter<T> getFilter(int aIndex) {
			return fFilters.get(aIndex);
		}

		@Override
		public void addChangeListener(ILcdChangeListener aListener) {
			fChangeSupport.addChangeListener(aListener);
		}

		@Override
		public void removeChangeListener(ILcdChangeListener aListener) {
			fChangeSupport.removeChangeListener(aListener);
		}

		private class MyChangeListener implements ILcdChangeListener {
			@Override
			public void stateChanged(TLcdChangeEvent aChangeEvent) {
				fChangeSupport.fireChangeEvent(new TLcdChangeEvent(CompositeAndFilter.this));
			}
		}
	}

}
