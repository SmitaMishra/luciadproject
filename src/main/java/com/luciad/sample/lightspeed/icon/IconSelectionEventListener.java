package com.luciad.sample.lightspeed.icon;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.luciad.model.ILcdModel;
import com.luciad.shape.ILcdPoint;
import com.luciad.shape.shape2D.TLcdLonLatPoint;
import com.luciad.shape.shape2D.TLcdLonLatPolyline;
import com.luciad.view.animation.ALcdAnimationManager;
import com.luciad.view.animation.ILcdAnimation;
import com.luciad.view.lightspeed.layer.ILspInteractivePaintableLayer;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class IconSelectionEventListener implements EventHandler<ActionEvent> {

	private final ILspInteractivePaintableLayer fLayer;
	private WeakReference<ILcdModel> fModel;
	private double fFlightSpeed;

	public IconSelectionEventListener(ILcdModel aModel, ILspInteractivePaintableLayer aLayer, double aFlightSpeed) {
		fLayer = aLayer;
		fModel = new WeakReference<ILcdModel>(aModel);
		fFlightSpeed = aFlightSpeed;
	}

	@Override
	public void handle(ActionEvent event) {
		TLcdLonLatPolyline polyline = null;
		Icon2DLonLatHeightPoint icon2D = null;

		List<Object> selected = fLayer.getSelectedObjects();
		for (Object o : selected) {
			if (o instanceof TLcdLonLatPolyline) {
				polyline = (TLcdLonLatPolyline) o;
			} else if (o instanceof Icon2DLonLatHeightPoint) {
				icon2D = (Icon2DLonLatHeightPoint) o;
			}
		}

		if (icon2D != null && polyline != null)
			animatePointAloneLine(fModel, icon2D, polyline, fFlightSpeed);
	}

	private void animatePointAloneLine(WeakReference<ILcdModel> aModel, Icon2DLonLatHeightPoint icon2D,
			TLcdLonLatPolyline polyline, double aFlightSpeed) {
		Icon2DAnimation animation = new Icon2DAnimation(aModel, icon2D, polyline, aFlightSpeed);
		ALcdAnimationManager.getInstance().putAnimation(icon2D, animation);
	}

	public class Icon2DAnimation implements ILcdAnimation {

		Icon2DLonLatHeightPoint fIcon2D;
		TLcdLonLatPolyline fPolyline;
		double fSpeed;
		WeakReference<ILcdModel> fModel;
		List<ILcdPoint> pointsList;
		int count = 0;

		public Icon2DAnimation(WeakReference<ILcdModel> aModel, Icon2DLonLatHeightPoint aIcon2D,
				TLcdLonLatPolyline aPolyline, double aSpeed) {
			fIcon2D = aIcon2D;
			fPolyline = aPolyline;
			fSpeed = aSpeed;
			fModel = aModel;

			ILcdPoint point1 = fPolyline.getStartPoint();
			ILcdPoint point2 = fPolyline.getEndPoint();

			double dx = point2.getX() - point1.getX();
			double dy = point2.getY() - point1.getY();

			pointsList = new ArrayList<ILcdPoint>();
			for (double x = point1.getX(); x < point2.getX(); x = x + (dx / 10)) {
				double y = point1.getY() + dy * (x - point1.getX()) / dx;
				pointsList.add(new TLcdLonLatPoint(x, y));
			}

			pointsList.add(point2);

		}

		@Override
		public double getDuration() {
			return fPolyline.getLength2D(0., 1.) / fFlightSpeed;
		}

		@Override
		public boolean isLoop() {
			return false;
		}

		@Override
		public void restart() {
			
			
		}

		@Override
		public void setTime(double aTime) {
			ILcdModel model = fModel.get();
			if (model == null) {
				// stop the animation if the model has gone
				ALcdAnimationManager.getInstance().removeAnimation(fIcon2D);
				return;

			}

			ILcdPoint point1 = fPolyline.getStartPoint();
			ILcdPoint point2 = fPolyline.getEndPoint();

			// interpolate position
			/*
			 * if (model.getModelReference() instanceof ILcdGeodeticReference) {
			 * ILcdGeodeticReference geoRef = (ILcdGeodeticReference)
			 * model.getModelReference(); ILcdEllipsoid ellipsoid =
			 * geoRef.getGeodeticDatum().getEllipsoid(); double angle =
			 * ellipsoid.rhumblineAzimuth2D(point1, point2);
			 * ellipsoid.geodesicPointSFCT(point1, fPolyline.getLength2D(0., 1.), angle,
			 * fIcon2D); }
			 */

			if (count < pointsList.size() - 1) {
				fIcon2D.move2D(pointsList.get(count++));
			} else if (count == pointsList.size() - 1) {
				fIcon2D.move2D(pointsList.get(pointsList.size() - 1));
			}

			if (fIcon2D.getX() == point2.getX() && fIcon2D.getY() == point2.getY())
				ALcdAnimationManager.getInstance().removeAnimation(fIcon2D);

			fModel.get().elementChanged(fIcon2D, ILcdModel.FIRE_NOW);
		}

		@Override
		public void start() {
			ILcdPoint point1 = fPolyline.getStartPoint();
			fIcon2D.move2D(point1.getX(), point1.getY());
			fModel.get().elementChanged(fIcon2D, ILcdModel.FIRE_NOW);
		}

		@Override
		public void stop() {

		}

	}

}
