package com.luciad.sample.lightspeed.utils;

import com.luciad.view.ILcdLayer;
import com.luciad.view.ILcdLayerTreeNode;
import com.luciad.view.lightspeed.layer.TLspInitialLayerIndexProvider;
import com.luciad.view.lightspeed.painter.grid.TLspLonLatGridLayerBuilder;

public class InitialLayerIndexProvider extends TLspInitialLayerIndexProvider {

	  @Override
	  public int getInitialLayerIndex(ILcdLayer aLayer, ILcdLayerTreeNode aLayerNode) {
	    int initialLayerIndex = super.getInitialLayerIndex(aLayer, aLayerNode);
	    if (aLayerNode.layerCount() > 0) {
	      if (isGridLayer(aLayer)) {
	        return aLayerNode.layerCount();
	      } else {
	        // check if the computed index would push down the (top) grid layer
	        ILcdLayer layer = aLayerNode.getLayer(aLayerNode.layerCount() - 1);
	        if (initialLayerIndex == aLayerNode.layerCount() &&
	            isGridLayer(layer)) {
	          return initialLayerIndex - 1;
	        }
	      }
	    }
	    return initialLayerIndex;
	  }

	  private boolean isGridLayer(ILcdLayer aLayer) {
	    return TLspLonLatGridLayerBuilder.GRID_TYPE_NAME.equals(aLayer.getModel().getModelDescriptor().getTypeName());
	  }
}
