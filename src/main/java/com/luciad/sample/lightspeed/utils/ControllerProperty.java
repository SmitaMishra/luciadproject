package com.luciad.sample.lightspeed.utils;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.luciad.view.lightspeed.ILspView;
import com.luciad.view.lightspeed.controller.ILspController;

import javafx.beans.property.BooleanPropertyBase;

public class ControllerProperty extends BooleanPropertyBase implements PropertyChangeListener {

	private final ILspView fView;
	private final ILspController fController;

	public ControllerProperty(ILspView aView, ILspController aController) {
		fView = aView;
		fController = aController;
		fView.addPropertyChangeListener(this);
	}

	@Override
	public Object getBean() {
		return fView;
	}

	@Override
	public String getName() {
		return fController.getName() + "Active";
	}

	@Override
	public void set(boolean aBoolean) {
		super.set(aBoolean);
		if (aBoolean) {
			fView.setController(fController);
		}
	}

	@Override
	public void propertyChange(final PropertyChangeEvent evt) {
		if ("controller".equals(evt.getPropertyName())) {
			if (!get() && fController == evt.getNewValue()) {
				ControllerProperty.super.set(true);
			} else if (get() && fController != evt.getNewValue()) {
				ControllerProperty.super.set(false);
			}
		}
	}
}
