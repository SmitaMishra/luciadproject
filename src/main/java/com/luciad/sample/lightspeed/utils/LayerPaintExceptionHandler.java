package com.luciad.sample.lightspeed.utils;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import com.luciad.util.ILcdChangeListener;
import com.luciad.util.ILcdChangeSource;
import com.luciad.util.TLcdChangeEvent;
import com.luciad.util.TLcdChangeSupport;
import com.luciad.view.ILcdLayer;
import com.luciad.view.ILcdPaintExceptionHandler;
import com.luciad.view.TLcdLoggingPaintExceptionHandler;

public class LayerPaintExceptionHandler implements ILcdPaintExceptionHandler, ILcdChangeSource {

	  private final Map<ILcdLayer, LogRecord> fExceptions = Collections.synchronizedMap(new WeakHashMap<ILcdLayer, LogRecord>());
	  private final ILcdPaintExceptionHandler fDelegate;
	  private final TLcdChangeSupport fChangeSupport = new TLcdChangeSupport();

	  public LayerPaintExceptionHandler() {
	    this(new TLcdLoggingPaintExceptionHandler());
	  }

	  public LayerPaintExceptionHandler(ILcdPaintExceptionHandler aDelegate) {
	    fDelegate = aDelegate;
	  }

	  @Override
	  public boolean handlePaintException(Exception aException, ILcdLayer aLayer) {
	    boolean result = fDelegate.handlePaintException(aException, aLayer);

	    if (aLayer != null) {
	      LogRecord value = new LogRecord(Level.SEVERE, aException.getLocalizedMessage());
	      value.setThrown(aException);
	      fExceptions.put(aLayer, value);
	      fChangeSupport.fireChangeEvent(new TLcdChangeEvent(this));
	    }

	    return result;
	  }

	  public Map<ILcdLayer, LogRecord> getExceptions() {
	    return fExceptions;
	  }

	  @Override
	  public void addChangeListener(ILcdChangeListener aListener) {
	    fChangeSupport.addChangeListener(aListener);
	  }

	  @Override
	  public void removeChangeListener(ILcdChangeListener aListener) {
	    fChangeSupport.removeChangeListener(aListener);
	  }

	  public void fireChangeEvent() {
	    fChangeSupport.fireChangeEvent(new TLcdChangeEvent(this));
	  }
}
