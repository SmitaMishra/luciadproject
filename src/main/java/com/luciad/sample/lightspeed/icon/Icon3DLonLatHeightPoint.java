package com.luciad.sample.lightspeed.icon;

import com.luciad.shape.shape2D.ILcd2DEditablePoint;
import com.luciad.shape.shape3D.ILcd3DEditableShape;
import com.luciad.shape.shape3D.TLcdLonLatHeightPoint;
import com.luciad.util.ILcd3DOrientationSettable;

public class Icon3DLonLatHeightPoint extends TLcdLonLatHeightPoint
		implements ILcd3DOrientationSettable, ILcd3DEditableShape, ILcd2DEditablePoint {

	private double fPitch;
	private double fYaw;
	private double fRoll;

	public Icon3DLonLatHeightPoint() {
		super();
	}

	public Icon3DLonLatHeightPoint(double aLon, double aLat, double aZ, double aPitch, double aYaw, double aRoll) {
		super(aLon, aLat, aZ);
		fPitch = aPitch;
		fYaw = aYaw;
		fRoll = aRoll;
	}

	////////////////////////////////
	// Orientation interface

	@Override
	public void setPitch(double aPitch) {
		fPitch = aPitch;
	}

	@Override
	public void setRoll(double aRoll) {
		fRoll = aRoll;
	}

	@Override
	public double getPitch() {
		return fPitch;
	}

	@Override
	public double getRoll() {
		return fRoll;
	}

	@Override
	public void setOrientation(double aOrientation) {
		fYaw = aOrientation;
	}

	@Override
	public double getOrientation() {
		return fYaw;
	}
}
