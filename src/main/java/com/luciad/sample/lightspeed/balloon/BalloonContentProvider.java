package com.luciad.sample.lightspeed.balloon;

import static com.luciad.util.concurrent.TLcdLockUtil.writeLock;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;

import com.luciad.model.ILcdModel;
import com.luciad.shape.ILcdPoint;
import com.luciad.shape.shape2D.ILcd2DEditablePoint;
import com.luciad.text.TLcdLonLatPointFormat;
import com.luciad.util.concurrent.TLcdLockUtil.Lock;
import com.luciad.view.swing.ALcdBalloonDescriptor;
import com.luciad.view.swing.ILcdBalloonContentProvider;
import com.luciad.view.swing.TLcdModelElementBalloonDescriptor;

public class BalloonContentProvider implements ILcdBalloonContentProvider {

	@Override
	public boolean canGetContent(ALcdBalloonDescriptor aBalloonDescriptor) {
		return aBalloonDescriptor.getObject() instanceof ILcdPoint;
	}

	@Override
	public JComponent getContent(ALcdBalloonDescriptor aBalloonDescriptor) {
		// Prepare a Swing panel to show the balloon in.
		final TLcdModelElementBalloonDescriptor balloonDescriptor = (TLcdModelElementBalloonDescriptor) aBalloonDescriptor;

		final JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		final ILcdPoint point = (ILcdPoint) aBalloonDescriptor.getObject();
		final TLcdLonLatPointFormat format = new TLcdLonLatPointFormat(TLcdLonLatPointFormat.DEC_DEG_3);
		final JFormattedTextField textField = new JFormattedTextField(format);
		final String[] previousValue = { format.format(point.getX(), point.getY()) };
		textField.setText(previousValue[0]);
		textField.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);

		// Used to move the point when editing the text field.
		textField.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("value".equals(evt.getPropertyName())) {
					Object value = textField.getValue();

					if (textField.isEditValid() && point instanceof ILcd2DEditablePoint && value instanceof ILcdPoint) {
						ILcdModel model = balloonDescriptor.getModel();
						try (Lock autoUnlock = writeLock(model)) {
							ILcd2DEditablePoint editablePoint = (ILcd2DEditablePoint) point;
							editablePoint.move2D((ILcdPoint) textField.getValue());
							model.elementChanged(editablePoint, ILcdModel.FIRE_NOW);
						}

						previousValue[0] = format.format(value);
					} else {
						textField.setText(previousValue[0]);
					}

					panel.requestFocusInWindow();
				}
			}
		});

		panel.add(textField);

		return panel;
	}
}
