<?xml version='1.0' encoding='UTF-8'?>
<FeatureTypeStyle xmlns="http://www.opengis.net/se" xmlns:ogc="http://www.opengis.net/ogc"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xsi:schemaLocation="http://www.opengis.net/ogc http://schemas.opengis.net/filter/1.1.0/filter.xsd   http://www.opengis.net/se http://schemas.opengis.net/se/1.1.0/FeatureStyle.xsd"
                  version="1.1.0">
  <Name>World</Name>
  <Description>
    <Title>World</Title>
    <Abstract>Visualizes objects as grey translucent areas.</Abstract>
  </Description>
  <Rule>
    <Name>body</Name>
    <Description>
      <Title>body rule</Title>
      <Abstract>rule to render the bodies</Abstract>
    </Description>
    <PolygonSymbolizer>
      <Fill>
        <SvgParameter name="fill">#53525C</SvgParameter>
        <SvgParameter name="fill-opacity">0.4</SvgParameter>
      </Fill>
      <Stroke>
        <SvgParameter name="stroke">#3E3E3E</SvgParameter>
        <SvgParameter name="stroke-width">1.5</SvgParameter>
        <SvgParameter name="stroke-opacity">1.0</SvgParameter>
      </Stroke>
    </PolygonSymbolizer>
  </Rule>
  <Rule>
    <Name>labels</Name>
    <Description>
	  <Title>label rule</Title>
      <Abstract>Labels the country with their name</Abstract>
    </Description>
    <TextSymbolizer>
      <Label>
        <ogc:PropertyName>COUNTRY</ogc:PropertyName>
      </Label>
      <Font>
        <SvgParameter name="font-family">Dialog</SvgParameter>
        <SvgParameter name="font-weight">bold</SvgParameter>
        <SvgParameter name="font-size">12</SvgParameter>
      </Font>
      <Halo>
        <Radius>1</Radius>
        <Fill>
          <SvgParameter name="fill">#000000</SvgParameter>
        </Fill>
      </Halo>
      <Fill>
        <SvgParameter name="fill">#ffffff</SvgParameter>
      </Fill>
    </TextSymbolizer>
  </Rule>
</FeatureTypeStyle>