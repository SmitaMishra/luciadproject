package com.luciad.sample.lightspeed.action;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import com.luciad.gui.ALcdAction;
import com.luciad.gui.ALcdActionWrapper;
import com.luciad.gui.ILcdAction;
import com.luciad.gui.TLcdActionAtLocationEvent;
import com.luciad.gui.swing.TLcdSWAction;
import com.luciad.util.logging.ILcdLogger;
import com.luciad.util.logging.TLcdLoggerFactory;

public class ShowPopupAction extends ALcdAction implements ILcdAction {

	private static ILcdLogger sLogger = TLcdLoggerFactory.getLogger(ShowPopupAction.class.getName());

	private ILcdAction[] fActionArray;
	private Component fComponent;

	public ShowPopupAction(ILcdAction[] aActionArray, Component aComponent) {
		super("PopupActionChoice");
		fActionArray = aActionArray;
		fComponent = aComponent;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (fComponent != null) {
			int x, y;
			if (e instanceof TLcdActionAtLocationEvent) {
				x = ((TLcdActionAtLocationEvent) e).getLocation().x;
				y = ((TLcdActionAtLocationEvent) e).getLocation().y;
			} else {
				x = fComponent.getWidth() / 2;
				y = fComponent.getHeight() / 2;
			}
			JPopupMenu menu = makePopupMenu(x, y);
			if (menu.getComponentCount() > 0) {
				menu.show(fComponent, x, y);
			}
		} else {
			sLogger.error("actionPerformed: cannot show popup on null component");
		}
	}

	protected JPopupMenu makePopupMenu(final int aX, final int aY) {
		final JPopupMenu menu = new JPopupMenu("Actions:");
		for (final ILcdAction action : fActionArray) {
			if (action == null) {
				menu.addSeparator();
			} else {
				if (action.getValue(ILcdAction.VISIBLE) != Boolean.valueOf(false)) {
					menu.add(new JMenuItem(new TLcdSWAction(new ALcdActionWrapper(action) {
						@Override
						public void actionPerformed(ActionEvent e) {
							// pass the location to the sub-actions
							TLcdActionAtLocationEvent atLocationEvent = new TLcdActionAtLocationEvent(e.getSource(),
									menu, e.getID(), e.getActionCommand(), e.getModifiers(), new Point(aX, aY));
							action.actionPerformed(atLocationEvent);
						}
					})));
				}
			}
		}
		return menu;
	}
}
