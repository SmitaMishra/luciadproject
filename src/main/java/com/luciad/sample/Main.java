
package com.luciad.sample;

import java.io.IOException;

import org.controlsfx.dialog.ExceptionDialog;

import com.luciad.sample.controller.MainController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		// Merge the Swing and JavaFX threads - needs Java 8
		System.setProperty("javafx.embed.singleThread", "true");

		// Verify that Direct3D is enabled (the default setting), to avoid resize
		// issues.
		if (Boolean.valueOf(System.getProperty("sun.java2d.noddraw", "false"))
				|| !Boolean.valueOf(System.getProperty("sun.java2d.d3d", "true"))) {
			throw new IllegalStateException("Do not change the Direct3D default settings (Windows specific), "
					+ "it may cause glitches during frame re-sizing.");
		}
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		// Disables the Swing lcd_touch library.
		System.setProperty("com.luciad.input.touch.windows.loadNativeDriver", "false");

		try {

			FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/main_page.fxml"));
			Parent root = loader.load();

			// Show the window
			primaryStage.setTitle("Cyclop Example");
			primaryStage.setScene(new Scene(root, 1280, 800)); // ability to add css to scene
			primaryStage.setResizable(false);
			primaryStage.setMaximized(false);
			primaryStage.show();

			// Clean-up the view when the window is closed
			primaryStage.setOnCloseRequest(event -> (((MainController) loader.getController()).getView()).destroy());

		} catch (IOException e) {
			e.printStackTrace();
			ExceptionDialog dialog = new ExceptionDialog(e);
			dialog.showAndWait();
		}
	}

}
