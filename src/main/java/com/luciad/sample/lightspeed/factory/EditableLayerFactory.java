/*
 *
 * Copyright (c) 1999-2018 Luciad All Rights Reserved.
 *
 * Luciad grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Luciad.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. LUCIAD AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL LUCIAD OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF LUCIAD HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */
package com.luciad.sample.lightspeed.factory;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;

import com.luciad.gui.TLcdImageIcon;
import com.luciad.model.ILcdModel;
import com.luciad.sample.lightspeed.icon.Icon2DLonLatHeightPoint;
import com.luciad.sample.lightspeed.icon.Icon3DLonLatHeightPoint;
import com.luciad.view.lightspeed.TLspContext;
import com.luciad.view.lightspeed.layer.ALspSingleLayerFactory;
import com.luciad.view.lightspeed.layer.ILspLayer;
import com.luciad.view.lightspeed.layer.TLspPaintState;
import com.luciad.view.lightspeed.layer.shape.TLspShapeLayerBuilder;
import com.luciad.view.lightspeed.painter.shape.TLspShapePaintingHints;
import com.luciad.view.lightspeed.style.ILspWorldElevationStyle;
import com.luciad.view.lightspeed.style.TLsp3DIconStyle;
import com.luciad.view.lightspeed.style.TLspFillStyle;
import com.luciad.view.lightspeed.style.TLspIconStyle;
import com.luciad.view.lightspeed.style.TLspIconStyle.ScalingMode;
import com.luciad.view.lightspeed.style.TLspLineStyle;
import com.luciad.view.lightspeed.style.styler.ALspStyleCollector;
import com.luciad.view.lightspeed.style.styler.ALspStyler;

public class EditableLayerFactory extends ALspSingleLayerFactory {

	@Override
	public boolean canCreateLayers(ILcdModel aModel) {
		return true;
	}

	@Override
	public ILspLayer createLayer(ILcdModel aModel) {

		IconStyler iconStyler = new IconStyler();

		return TLspShapeLayerBuilder.newBuilder().model(aModel).bodyEditable(true)
				.paintingHints(TLspShapePaintingHints.MAX_QUALITY).bodyStyler(TLspPaintState.REGULAR, iconStyler)
				.selectable(true).culling(false).build();
	}

	private class IconStyler extends ALspStyler {

		private final TLspIconStyle fIconStyle;
		private final TLsp3DIconStyle f3DIconStyle;
		private final TLspLineStyle fDefaultLineStyle;
		private final TLspFillStyle fDefaultFillStyle;

		public IconStyler() {
			// 2D Icon Style
			fIconStyle = TLspIconStyle.newBuilder().icon(new TLcdImageIcon("res/images/pin-red.png"))
					// Set icons to have fixed view coordinates
					.scalingMode(ScalingMode.VIEW_SCALING) // VIEW_SCALING for World View : WORLD_SCALING_CLAMPED
					// .worldSize(600)
					// .useOrientation(true)
					.scale(1.0).opacity(1.0f).build();

			// 3D Icon Style
			f3DIconStyle = TLsp3DIconStyle.newBuilder().icon("Data/3d_icons/car.obj").worldSize(600)
					.verticalOffsetFactor(1.0).iconSizeMode(TLsp3DIconStyle.ScalingMode.WORLD_SCALING)
					.elevationMode(ILspWorldElevationStyle.ElevationMode.ABOVE_TERRAIN).build();

			fDefaultLineStyle = TLspLineStyle.newBuilder().width(2).color(Color.white).build();

			fDefaultFillStyle = TLspFillStyle.newBuilder().color(Color.green).opacity(0.5f).build();
		}

		@Override
		public void style(Collection<?> aObjects, ALspStyleCollector aStyleCollector, TLspContext aStyle) {

			Collection<Object> icons2D = new ArrayList<Object>(aObjects.size());
			Collection<Object> icons3D = new ArrayList<Object>(aObjects.size());
			Collection<Object> shapes = new ArrayList<Object>(aObjects.size());
			for (Object object : aObjects) {
				if ((object instanceof Icon2DLonLatHeightPoint)) {
					icons2D.add((Icon2DLonLatHeightPoint) object);
					aStyleCollector.objects(icons2D).styles(fIconStyle).submit();
				} else if (object instanceof Icon3DLonLatHeightPoint) {
					icons3D.add((Icon3DLonLatHeightPoint) object);
					aStyleCollector.objects(icons3D).styles(f3DIconStyle).submit();
				} else {
					shapes.add(object);
					aStyleCollector.objects(shapes).styles(fDefaultLineStyle, fDefaultFillStyle).submit();
				}
			}
		}
	}
}
