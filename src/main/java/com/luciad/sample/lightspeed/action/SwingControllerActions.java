package com.luciad.sample.lightspeed.action;

import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JToggleButton;

import com.luciad.geodesy.TLcdGeodeticDatum;
import com.luciad.gui.ILcdAction;
import com.luciad.gui.TLcdRedoAction;
import com.luciad.gui.TLcdUndoAction;
import com.luciad.gui.TLcdUndoManager;
import com.luciad.gui.swing.TLcdSWAction;
import com.luciad.model.TLcdModelDescriptor;
import com.luciad.model.TLcdVectorModel;
import com.luciad.reference.TLcdGeodeticReference;
import com.luciad.sample.lightspeed.factory.ControllerFactory;
import com.luciad.sample.lightspeed.model.LonLatCreateControllerModel;
import com.luciad.sample.lightspeed.utils.LspDataUtil;
import com.luciad.util.ILcdFilter;
import com.luciad.view.ILcdLayeredListener;
import com.luciad.view.TLcdAWTEventFilterBuilder;
import com.luciad.view.TLcdLayeredEvent;
import com.luciad.view.lightspeed.ILspView;
import com.luciad.view.lightspeed.TLspSwingView;
import com.luciad.view.lightspeed.action.TLspSetControllerAction;
import com.luciad.view.lightspeed.controller.ALspController;
import com.luciad.view.lightspeed.controller.ILspController;
import com.luciad.view.lightspeed.controller.manipulation.TLspCreateController;
import com.luciad.view.lightspeed.controller.ruler.TLspRulerController;
import com.luciad.view.lightspeed.layer.ILspInteractivePaintableLayer;
import com.luciad.view.lightspeed.layer.ILspLayer;

public class SwingControllerActions {

	JButton textButton, D2PointButton, boundsButton, polylineButton, polyline3Button, rPolylineButton, polygonButton,
			cPolygonButton, circleButton, circle3Button, ellipseButton, arcButton, arc3Button, arcBulgeButton,
			arcCenterButton, arcBandButton, arcBand3Button, compCurveButton, compRingButton, varGeoBufferButton,
			bufferButton, geoBufferButton, lonBufferButton, sphereButton, domeButton;

	JButton rulerButton, redoButton, undoButton, recenterButton, defaultCursor;
	JButton pinButton;
	JToggleButton extrusionToggle;

	private ArrayList<LonLatCreateControllerModel> fControllerModels;
	private ILspInteractivePaintableLayer fCreationLayer;
	TLspSwingView fView;

	private static final TLcdUndoManager UNDO_MANAGER = new TLcdUndoManager();

	private ILspController fDefaultController;
	private TLspRulerController fRulerController;

	public SwingControllerActions(TLspSwingView aView, ILspInteractivePaintableLayer aCreationLayer) {

		fControllerModels = new ArrayList<>();
		// layer is used for creation controllers
		fCreationLayer = aCreationLayer;
		fView = aView;

		// Create default controllers & ruler controller
		createControllers();

		// Add recenter action
		recenterButton = addController(createRecenterController());

		// Add create controllers
		addCreateControllers();

		// extrusion toggle button
		extrusionToggle = addExtrusionToggleButton();

		// redo action & undo action
		TLcdUndoAction undoAction = new TLcdUndoAction(UNDO_MANAGER);
		TLcdRedoAction redoAction = new TLcdRedoAction(UNDO_MANAGER);
		undoButton = insertController(undoAction);
		redoButton = insertController(redoAction);
	}

	protected void createControllers() {
		fDefaultController = getDefaultController();
		defaultCursor = addController(fDefaultController);
		getView().setController(fDefaultController);
		fRulerController = createRulerController();
		if (fRulerController != null) {
			rulerButton = addController(fRulerController);
		}
	}

	public ILspController getDefaultController() {
		if (fDefaultController == null) {
			fDefaultController = createDefaultController();
		}
		return fDefaultController;
	}

	protected ILspController createDefaultController() {
		ILcdAction[] defaultControllerActions = createDefaultControllerActions();
		return ControllerFactory.createGeneralController(UNDO_MANAGER, fView, defaultControllerActions,
				defaultControllerActions[0], null, createStickyLabelsLayerFilter());
	}

	protected ILcdFilter<ILspLayer> createStickyLabelsLayerFilter() {
		return new ILcdFilter<ILspLayer>() {

			@Override
			public boolean accept(ILspLayer aObject) {
				return false;
			}
		};
	}

	public static Component getParentComponent(TLspSwingView aView) {
		return aView instanceof TLspSwingView ? ((TLspSwingView) aView).getHostComponent() : null;
	}

	protected ILcdAction[] createDefaultControllerActions() {
		ShowPropertiesAction propertiesAction = new ShowPropertiesAction(fView, getParentComponent(fView));
		return new ILcdAction[] { propertiesAction };
	}

	protected TLspRulerController createRulerController() {
		return ControllerFactory.createRulerController(UNDO_MANAGER);
	}

	protected ILspController createRecenterController() {
		return ControllerFactory.createRecenterProjectionController();
	}

	public JButton addController(ILspController aController) {
		return insertController(new TLspSetControllerAction(fView, aController));
	}

	private JButton insertController(ILcdAction aAction) {
		JButton button = new JButton();
		button.setMargin(new Insets(2, 2, 2, 2));
		TLcdSWAction swing_action = new TLcdSWAction(aAction);
		button.setAction(swing_action);

		swing_action.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (ILcdAction.VISIBLE.equals(evt.getPropertyName())) {
					button.setVisible((Boolean) evt.getNewValue());
				}
			}
		});
		return button;
	}

	private void addCreateControllers() {

		ILspInteractivePaintableLayer layer = getCreationLayer();

		textButton = addCreateController(layer, LonLatCreateControllerModel.Type.TEXT, null);
		D2PointButton = addCreateController(layer, LonLatCreateControllerModel.Type.POINT2D, null);
		boundsButton = addCreateController(layer, LonLatCreateControllerModel.Type.BOUNDS, null);
		polylineButton = addCreateController(layer, LonLatCreateControllerModel.Type.POLYLINE, null);
		rPolylineButton = addCreateController(layer, LonLatCreateControllerModel.Type.RHUMB_POLYLINE, null);
		polygonButton = addCreateController(layer, LonLatCreateControllerModel.Type.POLYGON, null);
		cPolygonButton = addCreateController(layer, LonLatCreateControllerModel.Type.COMPLEXPOLYGON, null);
		circleButton = addCreateController(layer, LonLatCreateControllerModel.Type.CIRCLE, null);
		circle3Button = addCreateController(layer, LonLatCreateControllerModel.Type.CIRCLE_BY_3_POINTS, null);
		ellipseButton = addCreateController(layer, LonLatCreateControllerModel.Type.ELLIPSE, null);
		arcButton = addCreateController(layer, LonLatCreateControllerModel.Type.ARC, null);
		arc3Button = addCreateController(layer, LonLatCreateControllerModel.Type.ARC_BY_3_POINTS, null);
		arcBulgeButton = addCreateController(layer, LonLatCreateControllerModel.Type.ARC_BY_BULGE, null);
		arcCenterButton = addCreateController(layer, LonLatCreateControllerModel.Type.ARC_BY_CENTER, null);
		arcBandButton = addCreateController(layer, LonLatCreateControllerModel.Type.ARCBAND, null);
		arcBand3Button = addCreateController(layer, LonLatCreateControllerModel.Type.ARC_BAND_3D, null);
		compCurveButton = addCreateController(layer, LonLatCreateControllerModel.Type.COMPOSITECURVE, null); // panelvisibilityaction
		compRingButton = addCreateController(layer, LonLatCreateControllerModel.Type.COMPOSITERING, null);
		bufferButton = addCreateController(layer, LonLatCreateControllerModel.Type.BUFFER, null);
		geoBufferButton = addCreateController(layer, LonLatCreateControllerModel.Type.GEOBUFFER, null);
		lonBufferButton = addCreateController(layer, LonLatCreateControllerModel.Type.LONLATHEIGHTBUFFER, null);
		varGeoBufferButton = addCreateController(layer, LonLatCreateControllerModel.Type.VARIABLE_GEO_BUFFER, null);
		polyline3Button = addCreateController(layer, LonLatCreateControllerModel.Type.POLYLINE_3D, null);
		domeButton = addCreateController(layer, LonLatCreateControllerModel.Type.DOME, null);
		sphereButton = addCreateController(layer, LonLatCreateControllerModel.Type.SPHERE, null);
		pinButton = addCreateController(layer, LonLatCreateControllerModel.Type.ICON_2D, null);
	}

	private JButton addCreateController(final ILspInteractivePaintableLayer aLayer,
			LonLatCreateControllerModel.Type aType, final ILcdAction aAction) {

		LonLatCreateControllerModel cm = new LonLatCreateControllerModel(aType, aLayer);
		fControllerModels.add(cm);

		// Create and initialize creation controller
		TLspCreateController createController = makeCreateController(cm, aAction);
		createController.addUndoableListener(UNDO_MANAGER);
		createController.setShortDescription(aType.toString());
		createController.setActionToTriggerAfterCommit(new TLspSetControllerAction(getView(), getDefaultController()));
		createController.setAWTFilter(TLcdAWTEventFilterBuilder.newBuilder().leftMouseButton().or().rightMouseButton()
				.or().keyEvents().build());
		ALspController navigation = ControllerFactory.createNavigationController();
		createController.appendController(navigation);

		final JButton createdButton = addController(createController);
		connectButtonToLayer(getView(), aLayer, createdButton);
		return createdButton;
	}

	private TLspCreateController makeCreateController(final LonLatCreateControllerModel aCreateControllerModel,
			final ILcdAction aInteractionAction) {
		TLspCreateController createController = new TLspCreateController(aCreateControllerModel) {
			@Override
			public void startInteraction(ILspView aView) {
				// Notify action of creation started
				if (aInteractionAction != null) {
					aInteractionAction
							.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Creation Started"));
				}
				super.startInteraction(aView);
			}

			@Override
			public void terminateInteraction(ILspView aView) {
				// Notify action of creation started
				if (aInteractionAction != null) {
					aInteractionAction
							.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Creation Stopped"));
				}
				super.terminateInteraction(aView);
			}
		};

		return createController;
	}

	public static void connectButtonToLayer(final ILspView aView, final ILspInteractivePaintableLayer aLayer,
			final JButton aCreatedButton) {
		aLayer.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if ("visible".equals(evt.getPropertyName())) {
					// Adjust the enabled state of the button based on the visibility of the layer
					aCreatedButton.setEnabled(aLayer.isVisible());
				}
			}
		});
		aView.addLayeredListener(new ILcdLayeredListener() {
			@Override
			public void layeredStateChanged(TLcdLayeredEvent e) {
				if (e.getID() == TLcdLayeredEvent.LAYER_REMOVED && e.getLayer() == aLayer) {
					// disable button when layer is removed from view
					aCreatedButton.setEnabled(false);
				} else if (e.getID() == TLcdLayeredEvent.LAYER_ADDED && e.getLayer() == aLayer) {
					// enable button when layer is added back to view
					aCreatedButton.setEnabled(true);
				}
			}
		});
	}

	public ILspInteractivePaintableLayer getCreationLayer() {
		if (fCreationLayer == null) {
			TLcdVectorModel emptyModel = new TLcdVectorModel(new TLcdGeodeticReference(new TLcdGeodeticDatum()));
			emptyModel.setModelDescriptor(
					new TLcdModelDescriptor("Layer containing the newly created shapes.", "Shapes", "Geodetic Shapes"));
			fCreationLayer = (ILspInteractivePaintableLayer) LspDataUtil.instance().model(emptyModel).layer()
					.editable(true).getLayer();
		}
		return fCreationLayer;
	}

	protected JToggleButton addExtrusionToggleButton() {
		final JToggleButton button = new JToggleButton();
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final boolean extruded = button.isSelected();
				setCreateExtrudedShapes(extruded);
			}
		});
		return button;
	}

	protected void setCreateExtrudedShapes(boolean aExtruded) {
		for (LonLatCreateControllerModel controllerModel : fControllerModels) {
			controllerModel.setCreateExtrudedShape(aExtruded);
		}
	}

	public ILspView getView() {
		return fView;
	}

	/*
	 * Buttons
	 */

	public JToggleButton getExtrusionButton() {
		return extrusionToggle;
	}

	public JButton getTextButton() {
		return textButton;
	}

	public JButton getPinButton() {
		return pinButton;
	}

	public JButton getBoundsButton() {
		return boundsButton;
	}

	public JButton getPolylineButton() {
		return polylineButton;
	}

	public JButton getPolyline3Button() {
		return polyline3Button;
	}

	public JButton getrPolylineButton() {
		return rPolylineButton;
	}

	public JButton getPolygonButton() {
		return polygonButton;
	}

	public JButton getcPolygonButton() {
		return cPolygonButton;
	}

	public JButton getCircleButton() {
		return circleButton;
	}

	public JButton getCircle3Button() {
		return circle3Button;
	}

	public JButton getEllipseButton() {
		return ellipseButton;
	}

	public JButton getArcButton() {
		return arcButton;
	}

	public JButton getArc3Button() {
		return arc3Button;
	}

	public JButton getArcBulgeButton() {
		return arcBulgeButton;
	}

	public JButton getArcCenterButton() {
		return arcCenterButton;
	}

	public JButton getArcBandButton() {
		return arcBandButton;
	}

	public JButton getArcBand3Button() {
		return arcBand3Button;
	}

	public JButton getCompCurveButton() {
		return compCurveButton;
	}

	public JButton getCompRingButton() {
		return compRingButton;
	}

	public JButton getVarGeoBufferButton() {
		return varGeoBufferButton;
	}

	public JButton getBufferButton() {
		return bufferButton;
	}

	public JButton getGeoBufferButton() {
		return geoBufferButton;
	}

	public JButton getLonBufferButton() {
		return lonBufferButton;
	}

	public JButton getSphereButton() {
		return sphereButton;
	}

	public JButton getDomeButton() {
		return domeButton;
	}

	public JButton getRecenterButton() {
		return recenterButton;
	}

	public JButton getRulerButton() {
		return rulerButton;
	}

	public JButton getUndoButton() {
		return undoButton;
	}

	public JButton getRedoButton() {
		return redoButton;
	}

	public JButton getDefaultCursor() {
		return defaultCursor;
	}

	public JButton get2DPointButton() {
		return D2PointButton;
	}
}
