/*
 *
 * Copyright (c) 1999-2018 Luciad All Rights Reserved.
 *
 * Luciad grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Luciad.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. LUCIAD AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL LUCIAD OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF LUCIAD HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */
package com.luciad.sample.lightspeed.utils;

import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;

import javafx.beans.property.BooleanPropertyBase;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

import com.luciad.gui.ILcdIcon;
import com.luciad.gui.TLcdIconFactory;
import com.luciad.util.TLcdNoBoundsException;
import com.luciad.util.TLcdOutOfBoundsException;
import com.luciad.view.ILcdLayer;
import com.luciad.view.ILcdLayerTreeNode;
import com.luciad.view.ILcdLayeredListener;
import com.luciad.view.lightspeed.ILspAWTView;
import com.luciad.view.lightspeed.layer.ILspLayer;
import com.luciad.view.lightspeed.util.TLspViewNavigationUtil;

/**
 * A JavaFX based layer control implementation.
 */
public class FXLayerControl extends TreeView<ILcdLayer> {

	private final ILspAWTView fView;

	public FXLayerControl(ILspAWTView aView) {
		super(null);

		fView = aView;
		buildTree();
		fView.addLayeredListener((ILcdLayeredListener) e -> buildTree());
	}

	private void buildTree() {
		ILcdLayerTreeNode root = fView.getRootNode();
		TreeItem<ILcdLayer> rootNode = handleLayer(null, root);
		setRoot(rootNode);
		setCellFactory(aView -> new CustomTreeCell());
		rootNode.setExpanded(true);
		setShowRoot(false);
	}

	private TreeItem<ILcdLayer> handleLayer(TreeItem<ILcdLayer> aNode, ILcdLayer aLayer) {
		TreeItem<ILcdLayer> treeItem = new TreeItem<>(aLayer);
		if (aNode != null) {
			aNode.getChildren().add(treeItem);
		}
		if (aLayer instanceof ILcdLayerTreeNode) {
			ILcdLayerTreeNode layerTreeNode = (ILcdLayerTreeNode) aLayer;
			for (int i = 0; i < layerTreeNode.layerCount(); i++) {
				ILcdLayer layer = layerTreeNode.getLayer(i);
				handleLayer(treeItem, layer);
			}
		}
		return treeItem;
	}

	public static Node createImageView(ILcdIcon aIcon) {
		BufferedImage bImage = new BufferedImage(aIcon.getIconWidth(), aIcon.getIconHeight(),
				BufferedImage.TYPE_INT_ARGB);
		aIcon.paintIcon(null, bImage.getGraphics(), 0, 0);
		Image fxImage = SwingFXUtils.toFXImage(bImage, null);
		return new ImageView(fxImage);
	}

	private class CustomTreeCell extends TreeCell<ILcdLayer> {

		private final CheckBox checkBox = new CheckBox();
		private final Label label = new Label("");
		private final Button fitButton = new Button(null,
				createImageView(TLcdIconFactory.create(TLcdIconFactory.FIT_ICON)));
		private final Button removeButton = new Button(null,
				createImageView(TLcdIconFactory.create(TLcdIconFactory.DELETE_ICON)));

		private final HBox hBox;

		private ILcdLayer layer;
		private LayerVisibility layerProperty;

		public CustomTreeCell() {
			fitButton.setVisible(false);
			removeButton.setVisible(false);
			hBox = new HBox();
			Region spacer = new Region();
			HBox.setHgrow(spacer, Priority.ALWAYS);
			hBox.getChildren().addAll(checkBox, label, spacer, fitButton, removeButton);

			setOnMouseEntered(aEvent -> {
				fitButton.setVisible(true);
				removeButton.setVisible(true);
			});

			setOnMouseExited(aEvent -> {
				fitButton.setVisible(false);
				removeButton.setVisible(false);
			});

			fitButton.setOnAction(aEvent -> {
				if (layer instanceof ILspLayer) {
					try {
						new TLspViewNavigationUtil(fView).animatedFit(Collections.singleton((ILspLayer) layer));
					} catch (TLcdNoBoundsException | TLcdOutOfBoundsException e) {
						e.printStackTrace();
					}
				}
			});

			removeButton.setOnAction(aEvent -> {
				fView.removeLayer(layer);
			});
		}

		@Override
		protected void updateItem(ILcdLayer aLayer, boolean aEmpty) {
			super.updateItem(aLayer, aEmpty);
			if (aEmpty) {
				layer = null;
				if (layerProperty != null) {
					checkBox.selectedProperty().unbindBidirectional(layerProperty);
				}
				setGraphic(null);
			} else {
				layer = aLayer;
				layerProperty = new LayerVisibility(layer);
				// Note, this binding will update the checkbox on the correct thread. Properties
				// can be changed on any thread.
				checkBox.selectedProperty().bindBidirectional(layerProperty);
				label.setText(aLayer.getLabel());
				setGraphic(hBox);
			}
		}
	}

	private static class LayerVisibility extends BooleanPropertyBase implements PropertyChangeListener {

		private final ILcdLayer fLayer;

		public LayerVisibility(ILcdLayer aLayer) {
			fLayer = aLayer;
			fLayer.addPropertyChangeListener(this);
			super.set(aLayer.isVisible());
		}

		@Override
		public Object getBean() {
			return fLayer;
		}

		@Override
		public String getName() {
			return fLayer.getLabel() + "Visible";
		}

		@Override
		public void set(final boolean aBoolean) {
			super.set(aBoolean);
			fLayer.setVisible(aBoolean);
		}

		@Override
		public void propertyChange(final PropertyChangeEvent evt) {
			if ("visible".equals(evt.getPropertyName())) {
				if (!get() && (boolean) evt.getNewValue()) {
					LayerVisibility.super.set(true);
				} else if (get() && !(boolean) evt.getNewValue()) {
					LayerVisibility.super.set(false);
				}
			}
		}
	}
}
